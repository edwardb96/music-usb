from playlist import Playlist, save_playlist
from pathlib import Path
from async_worker import run_async


class PlaylistsController(object):
    def __init__(self, view, parent, player, songs):
        self.view = view
        self.view.subscribe(self)

        self.selected_drive_path = None
        self.parent = parent
        self.songs = songs
        self.player = player

        self.playlists = {}
        self.selected_song_index = None
        self.selected_playlist_name = None
        self.selected_playlist = None

    def enable_new_playlist(self):
        self.view.enable_new_playlist()

    def disable_new_playlist(self):
        self.view.disable_new_playlist()

    def on_new_playlist(self):
        name = self.view.ask_playlist_name()
        if name and name not in self.playlists:
            new_playlist = Playlist(name, Path(name + ".m3u"))
            self.playlists[name] = new_playlist
            self.songs.set_playlist_names(list(self.playlists.keys()))
            self.view.add_playlist(name)

            playlist_path = \
                self.selected_drive_path / new_playlist.relative_path

            def on_success(_):
                self.parent.unlock_drive()
                self.parent.end_prevent_writes()
                self.parent.end_progress_floating()
                self.parent.set_status("")

            self.parent.lock_drive()
            self.parent.begin_prevent_writes()
            self.parent.begin_progress_floating()
            self.parent.set_status(
                "Making empty playlist named '{}'...".format(name))
            run_async(lambda _: playlist_path.touch(),
                      on_success=on_success,
                      on_fail=self.parent.handle_async_error)

    def on_selected_song_changed(self, new_song):
        if new_song is None:
            self.selected_song_index = None
            self.player.pause()
        else:
            self.selected_song_index = new_song
            song = self.selected_playlist[self.selected_song_index]
            self.songs.select_song_by_path(song)

    def on_next(self):
        assert self.selected_playlist is not None
        assert self.selected_song_index is not None
        self.view.select_next()

    def on_previous(self):
        assert self.selected_playlist is not None
        assert self.selected_song_index is not None
        self.view.select_previous()

    def begin_prevent_writes(self):
        self.view.disable_new_playlist()
        self.view.disable_reorder()
        self.view.disable_remove()

    def end_prevent_writes(self, is_active):
        self.view.enable_new_playlist()
        self.view.enable_reorder()
        if self.selected_playlist is not None:
            self.view.enable_remove()
        if is_active and self.selected_playlist:
            self.player.enable_remove_media()

    def begin_prevent_reads(self):
        self.view.disable_songs_list()
        self.view.disable_playlist_list()

    def end_prevent_reads(self, is_active):
        self.view.enable_songs_list()
        self.view.enable_playlist_list()
        if is_active and self.selected_playlist:
            self.player.enable_controls()

    def begin_switch_drive(self):
        self.__begin_reset_playlists()
        self.selected_drive_path = None

    def end_switch_drive(self, drive_path, playlists, is_active):
        self.selected_drive_path = drive_path
        self.__end_reset_playlists(playlists, is_active)

    def begin_restore(self):
        self.__begin_reset_playlists()

    def end_restore(self, playlists, is_active):
        self.__end_reset_playlists(playlists, is_active)

    def __begin_reset_playlists(self):
        self.begin_prevent_reads()
        self.begin_prevent_writes()
        self.view.clear_playlists()
        self.view.clear_songs()
        self.songs.set_playlist_names([])

        self.playlists = None
        self.selected_playlist = None
        self.selected_playlist_name = None
        self.selected_song_index = None

    def __end_reset_playlists(self, playlists, is_active):
        self.playlists = playlists
        self.songs.set_playlist_names(list(playlists.keys()))
        for playlist_name in playlists.keys():
            self.view.add_playlist(playlist_name)
        self.end_prevent_reads(is_active)
        self.end_prevent_writes(is_active)

    def async_write_playlist(self, playlist):
        def on_success():
            self.parent.unlock_drive()
            self.parent.end_prevent_writes()
            self.parent.end_progress_floating()
            self.parent.set_status("")

        self.parent.lock_drive()
        self.parent.begin_prevent_writes()
        self.parent.begin_progress_floating()
        self.parent.set_status("Writing to playlist '{}'...".format(playlist.name))

        run_async(lambda _: save_playlist(self.selected_drive_path, playlist),
                  on_success,
                  on_fail=self.parent.handle_async_error)

    def on_selected_playlist_changed(self, new_key):
        if new_key is not None:
            self.selected_playlist_name = new_key
            self.selected_playlist = self.playlists[new_key]
            self.view.set_songs(self.selected_playlist)

            if not self.parent.prevent_writes:
                self.view.enable_remove()
                if self.selected_playlist:
                    self.player.enable_remove_media()
                else:
                    self.player.disable_remove_media()

            if self.selected_playlist:
                self.view.select_first_song()
                self.player.enable_controls()
            else:
                self.player.disable_controls()
        else:
            self.selected_playlist_name = None
            self.selected_playlist = None
            self.selected_song_index = None
            self.view.disable_remove()

            self.player.disable_controls()
            self.player.disable_remove_media()

    def focus(self):
        if self.selected_playlist:
            if not self.parent.prevent_reads:
                self.player.enable_controls()

            if not self.parent.prevent_writes:
                self.player.enable_remove_media()
        else:
            self.player.disable_controls()
            self.player.disable_remove_media()

        if self.selected_song_index is not None:
            song = self.selected_playlist[self.selected_song_index]
            self.songs.select_song_by_path(song)

    def on_add_to_playlist(self, playlist_name, song):
        playlist = self.playlists[playlist_name]
        was_empty = bool(playlist)
        playlist.append(song)
        song.added_to_playlist(playlist)
        self.async_write_playlist(playlist)
        if playlist_name == self.selected_playlist_name:
            self.view.add_song(song)
            if was_empty:
                self.view.select_first_song()
            # player is enabled by end to writing if suitable

    def prepare_remove_song(self, song):
        self.parent.lock_drive()
        self.parent.begin_prevent_writes()
        self.parent.begin_progress_floating()
        self.parent.set_status("Removing the song {} from playlists..."
                               .format(song.metadata.track_title))

        def remove_references_to_song(song):
            ui_indices_to_remove = None

            for playlist_name in song.used_in_playlists():
                playlist = self.playlists[playlist_name]
                indices = list(playlist.remove_uses_of_song(song))
                if indices:
                    save_playlist(self.selected_drive_path, playlist)
                if playlist_name == self.selected_playlist_name:
                    ui_indices_to_remove = indices

            return ui_indices_to_remove

        def on_success(indices_to_remove):
            for i in indices_to_remove:
                self.view.remove_at(i)
            self.parent.unlock_drive()
            self.parent.end_prevent_writes()
            self.parent.end_progress_floating()
            self.parent.set_status("")

        if not self.selected_playlist:
            self.player.disable_controls()
            self.player.disable_remove_media()

        run_async(lambda _: remove_references_to_song(song),
                  on_success,
                  on_fail=self.parent.handle_async_error)

    def on_remove(self):
        song = self.selected_playlist.pop(self.selected_song_index)
        self.view.remove_selected_song()

        song.removed_from_playlist(self.selected_playlist)
        self.async_write_playlist(self.selected_playlist)

        if not self.selected_playlist:
            self.player.disable_controls()
            self.player.disable_remove_media()

    def on_reorder(self, from_, to):
        self.selected_playlist.reorder(from_, to)
        self.async_write_playlist(self.selected_playlist)

    def on_remove_playlist(self):
        if self.view.ask_remove_playlist(self.selected_playlist_name):
            removed_playlist = self.playlists.pop(self.selected_playlist_name)
            removed_playlist.before_remove()

            if not self.playlists:
                self.view.clear_songs()

            self.songs.set_playlist_names(list(self.playlists.keys()))

            self.selected_playlist_name = None
            self.selected_playlist = None
            self.selected_song_index = None
            self.view.remove_selected_playlist()

            self.parent.lock_drive()
            self.parent.begin_prevent_writes()
            self.parent.set_status("Removing playlist '{}'...".format(removed_playlist.name))

            playlist_drive_path = \
                self.selected_drive_path / removed_playlist.relative_path

            def on_success(_):
                self.parent.unlock_drive()
                self.parent.set_status("")
                self.parent.end_prevent_writes()

            run_async(lambda _: playlist_drive_path.unlink(),
                      on_success,
                      self.parent.handle_async_error)
