from music.metadata import try_album_art
from music.media_player import VlcMp3Player


class PlayerController(object):
    def __init__(self, view):
        self.view = view
        self.view.subscribe(self)
        self.song = None

        self.vlc = VlcMp3Player(view, self.on_state_changed)
        self.selected_drive_path = None

    def subscribe(self, songs):
        self.on_next_callback = songs.on_next
        self.on_previous_callback = songs.on_previous
        self.on_remove_callback = songs.on_remove

    def set_selected_drive(self, selected_drive_path):
        self.selected_drive_path = selected_drive_path

    def enable_controls(self):
        self.view.enable_controls()

    def disable_controls(self):
        self.view.disable_controls()

    def enable_remove_media(self):
        self.view.enable_remove_media()

    def disable_remove_media(self):
        self.view.disable_remove_media()

    def pause(self):
        self.vlc.pause()

    def play(self):
        self.vlc.play()

    def is_playing(self):
        self.vlc.play()

    def set_song(self, song):
        if song and ((not self.song) or song != self.song):
            self.song = song
            was_playing = self.vlc.is_playing()
            song_drive_path = self.selected_drive_path / song.relative_path

            self.view.set_track_title(song.metadata.track_title)
            self.view.set_album(song.metadata.album)
            self.view.set_artist(song.metadata.artist or song.metadata.album_artist)

            album_art = try_album_art(song_drive_path)
            if album_art is not None:
                self.view.set_album_art(album_art)
            else:
                self.view.reset_album_art()

            self.vlc.set_media(str(song_drive_path))
            if was_playing:
                self.vlc.play()
        else:
            self.vlc.pause()

    def on_play_pause(self):
        if self.vlc.is_playing():
            self.vlc.pause()
        else:
            self.vlc.play()

    def on_remove(self):
        self.on_remove_callback()

    def on_previous(self):
        self.on_previous_callback()

    def on_next(self, silent=False):
        self.on_next_callback()

    def on_state_changed(self, is_playing):
        if is_playing:
            self.view.show_pause_button()
        else:
            self.view.show_play_button()
