from os import stat
from functools import partial
from partition import partition

from async_worker import run_async
from pathlib import Path

from files import delete_up_until_not_empty
from files.progress import copy_files_with_progress
from song import Song
from music import try_song_from_library


class SongsController(object):
    def __init__(self, view, parent, player):
        self.view = view
        self.parent = parent
        self.player = player
        self.view.subscribe(self)
        self.songs_index = None
        self.selected_drive_path = None

    def begin_prevent_writes(self):
        self.view.disable_add_media()
        self.view.disable_add_to_playlist()

    def end_prevent_writes(self, is_active):
        self.view.enable_add_media()
        self.view.enable_add_to_playlist()
        if is_active and self.songs_index:
            self.player.enable_remove_media()

    def begin_prevent_reads(self):
        self.view.disable_songs_list()

    def end_prevent_reads(self, is_active):
        self.view.enable_songs_list()
        if is_active and self.songs_index:
            self.player.enable_controls()

    def begin_switch_drive(self):
        self.__begin_change_songs()
        self.selected_drive_path = None

    def end_switch_drive(self, drive_path, songs, is_active):
        self.selected_drive_path = drive_path
        self.__end_change_songs(songs, is_active)

    def begin_restore(self):
        self.__begin_change_songs()

    def end_restore(self, songs, is_active):
        self.__end_change_songs(songs, is_active)

    def __begin_change_songs(self):
        self.begin_prevent_reads()
        self.begin_prevent_writes()
        self.view.clear_songs()
        self.view.clear_filter()
        self.songs_index = None

    def __end_change_songs(self, songs, is_active):
        (songs_list, self.songs_index) = songs
        self.view.add_songs(songs_list)
        self.end_prevent_reads(is_active)
        self.end_prevent_writes(is_active)
        if self.songs_index:
            self.view.select_first()

    def on_sort_changed(self, n):
        if self.selected_song:
            self.selected_song = n

    def on_add_media_directory(self):
        directory_path = self.view.choose_album_dialog()
        if directory_path:
            self.add_files_async([
                str(p) for p in Path(directory_path).glob("**/*.mp3")])

    def on_add_media_files(self):
        path_strings = self.view.choose_tracks_dialog()
        if path_strings:
            self.add_files_async(path_strings)

    def on_selected_changed(self, new_song):
        if new_song is None:
            self.selected_song = None
            self.player.pause()
        else:
            self.selected_song = new_song
            song = self.songs_index[self.selected_song]
            self.player.set_song(song)

    def select_song_by_path(self, song):
        self.view.select_song_by_path(str(song.relative_path))

    def on_add_to_playlist(self, playlist_key, song_key):
        self.parent.on_add_to_playlist(playlist_key, self.songs_index[song_key])

    def set_playlist_names(self, playlist_names):
        self.view.set_playlist_names(playlist_names)

    def on_previous(self):
        assert self.selected_song is not None
        self.view.select_previous()

    def on_next(self, silent=False):
        assert self.selected_song is not None
        self.view.select_next()

    def add_files_async(self, path_strings):
        # async create songs, except for the ones which fail
        def create_song_metadata(_):
            return partition(
                lambda x: isinstance(x, Song),
                (try_song_from_library(Path(p)) for p in path_strings))

        def on_songs_created(songs_and_rejects):
            (songs, rejects) = songs_and_rejects
            metadatas = (self.choose_metadata_dialog(from_id3, from_path)
                         for from_id3, from_path in rejects)

            songs.extend(Song(metadata) for metadata in metadatas
                         if metadata is not None)

            def copy_songs(songs, report):
                worklist = []
                copied_songs = []
                for i, song in enumerate(songs):
                    song_drive_path = \
                        self.selected_drive_path / song.relative_path
                    if not song_drive_path.is_file():
                        song_drive_path.parent.mkdir(parents=True,
                                                     exist_ok=True)
                        copied_songs.append(song)
                        worklist.append((path_strings[i], str(song_drive_path)))

                copy_files_with_progress(worklist, report)
                return (copied_songs, {str(s.relative_path): s for s in copied_songs})

            def on_success(songs):
                (new_song_list, new_songs_index) = songs
                was_previously_empty = not bool(self.songs_index)
                self.songs_index.update(new_songs_index)

                self.view.add_songs(new_song_list)

                self.parent.unlock_drive()
                self.parent.enable_backup_actions()
                self.parent.end_prevent_writes()
                self.parent.set_status("")
                self.parent.end_progress()

                if was_previously_empty and self.songs_index:
                    self.player.enable_controls()

            run_async(partial(copy_songs, songs),
                      on_success=on_success,
                      on_fail=self.parent.handle_async_error,
                      on_progress=self.parent.report_progress)

        def total_copy_size(path_strings):
            return sum(map(lambda p: stat(p).st_size, path_strings))

        self.parent.lock_drive()
        self.parent.disable_backup_actions()
        self.parent.begin_prevent_writes()
        self.parent.set_status("Adding music to drive...")
        self.parent.begin_progress(total_copy_size(path_strings))

        run_async(create_song_metadata,
                  on_success=on_songs_created,
                  on_fail=self.parent.handle_async_error)

    def on_filter_changed(self, new_text):
        self.view.set_filter(new_text)

    def on_clear_search(self):
        self.view.clear_filter()

    def on_remove(self):
        song = self.songs_index[self.selected_song]
        if not song.is_used_in_playlists() or \
           self.view.ask_should_remove_song():

            if song.is_used_in_playlists():
                self.parent.prepare_remove_song(song)

            song = self.songs_index.pop(self.selected_song)
            self.view.remove_current()

            if self.songs_index:
                if not self.view.filtered_has_results():
                    self.view.clear_filter()
                    self.view.select_first()
            else:
                self.player.disable_controls()

            def remove(_):
                song_drive_path = self.selected_drive_path / song.relative_path
                delete_up_until_not_empty(song_drive_path)

            def on_success(_):
                self.parent.unlock_drive()
                self.parent.end_prevent_writes()
                self.parent.enable_backup_actions()
                self.parent.set_status("")

            # Asyncronously delete the file from the drive and backup.
            self.parent.lock_drive()
            self.parent.begin_prevent_writes()
            self.parent.disable_backup_actions()
            self.parent.set_status("Removing {}".format(song.relative_path))
            run_async(remove, on_success, self.parent.handle_async_error)

    def focus(self):
        if self.songs_index:
            if not self.parent.prevent_reads:
                self.player.enable_controls()

            if not self.parent.prevent_writes:
                self.player.enable_remove_media()
        else:
            self.player.disable_controls()
            self.player.disable_remove_media()
