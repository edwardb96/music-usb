from sys import exit
from pathlib import Path
from functools import partial
from datetime import datetime
from shutil import move

from async_worker import run_async

from files.sync import sync_and_cost, run_instructions_with_progress
from files import move_clear

from music.metadata import song_from_semantic_filepath
from playlist import save_playlist, load_playlist

from .metadata import MetadataController
from .playlists import PlaylistsController
from .player import PlayerController
from .songs import SongsController


class Controller(object):
    def __init__(self, view):
        self.view = view
        self.view.subscribe(self)
        self.selected_backup_path = None
        self.selected_drive_path = None

        self.player = PlayerController(self.view.player)
        self.songs = SongsController(self.view.songs,
                                     self, self.player)
        self.playlists = PlaylistsController(self.view.playlists,
                                             self, self.player, self.songs)

        self.current_tab = self.songs
        self.player.subscribe(self.current_tab)

        self.prevent_writes = False
        self.prevent_writes_and_reads = False

    def handle_async_error(self, error):
        (exctype, value, traceback_str) = error
        with open("crash.log", "a+") as error_log:
            # log exception
            date_time_string = datetime.now().strftime("%I:%M%p on %B %d, %Y")
            error_log.write("## error on background thread - {}\n"
                            .format(date_time_string))
            error_log.write(traceback_str + "\n")
        exit(1)

    def on_add_to_playlist(self, playlist_name, song):
        self.playlists.on_add_to_playlist(playlist_name, song)

    def on_select_drive(self):
        drive_path_string = self.view.choose_drive_dialog()
        if drive_path_string:
            self.set_drive_async(Path(drive_path_string))

    def on_restore(self):
        self.set_status("Restoring from backup...")
        self.begin_restore()

        def on_complete(load_result):
            (songs_list, songs_index, playlists_index) = load_result
            self.set_status('')
            self.end_restore((songs_list, songs_index), playlists_index)

        self.async_synchronize(
            str(self.selected_backup_path),
            str(self.selected_drive_path),
            lambda: self.async_load_drive_contents([], [], on_complete))

    def on_backup(self):
        self.set_status("Backing Up...")
        self.begin_backup()

        def on_complete():
            self.set_status('')
            self.end_backup()

        self.async_synchronize(str(self.selected_drive_path),
                               str(self.selected_backup_path),
                               on_complete)

    def async_synchronize(self, master_dir, slave_dir, on_complete):
        def on_computed_diff(diff):
            (instructions, cost) = diff
            self.view.end_progress_floating()

            def on_sync_complete(_):
                on_complete()
                self.end_progress()

            if cost > 0:
                self.view.begin_progress(cost)
                run_async(partial(
                          run_instructions_with_progress, instructions),
                          on_sync_complete,
                          self.handle_async_error,
                          self.view.report_progress)
            else:
                on_complete()

        self.view.begin_progress_floating()
        run_async(lambda _: sync_and_cost(master_dir, slave_dir),
                  on_computed_diff,
                  self.handle_async_error)

    def begin_prevent_reads(self):
        self.__begin_prevent_reads()
        self.songs.begin_prevent_reads()
        self.playlists.begin_prevent_reads()

    def end_prevent_reads(self):
        self.__end_prevent_reads()
        self.songs.end_prevent_reads(is_active=self.songs == self.current_tab)
        self.playlists.end_prevent_reads(
            is_active=self.playlists == self.current_tab)

    def begin_prevent_writes(self):
        self.__begin_prevent_writes()
        self.songs.begin_prevent_writes()
        self.playlists.begin_prevent_writes()

    def end_prevent_writes(self):
        self.__end_prevent_writes()
        self.songs.end_prevent_writes(is_active=self.songs == self.current_tab)
        self.playlists.end_prevent_writes(
            is_active=self.playlists == self.current_tab)

    def begin_backup(self):
        self.__begin_prevent_writes()
        self.lock_drive_and_backup()
        self.disable_backup_actions()

        self.songs.begin_prevent_writes()
        self.playlists.begin_prevent_writes()

    def end_backup(self):
        self.__end_prevent_writes()
        self.unlock_drive_and_backup()
        self.enable_backup_actions()

        self.songs.end_prevent_writes(
            is_active=self.songs == self.current_tab)
        self.playlists.end_prevent_writes(
            is_active=self.playlists == self.current_tab)

    def begin_restore(self):
        self.__begin_prevent_reads()
        self.__begin_prevent_writes()
        self.lock_drive_and_backup()
        self.disable_backup_actions()

        self.songs.begin_restore()
        self.playlists.begin_restore()

    def end_restore(self, songs, playlists):
        self.__end_prevent_reads()
        self.__end_prevent_writes()
        self.unlock_drive_and_backup()
        self.enable_backup_actions()

        self.songs.end_restore(
            songs, is_active=self.songs == self.current_tab)
        self.playlists.end_restore(
            playlists, is_active=self.playlists == self.current_tab)

    def begin_switch_drive(self):
        self.__begin_prevent_reads()
        self.__begin_prevent_writes()
        self.lock_drive_and_backup()
        self.disable_backup_actions()

        self.view.set_backup_location('')
        self.selected_backup_path = None

        self.songs.begin_switch_drive()
        self.playlists.begin_switch_drive()

    def end_switch_drive(self, songs, playlists):
        self.__end_prevent_reads()
        self.__end_prevent_writes()
        self.unlock_drive_and_backup()

        self.songs.end_switch_drive(
            self.selected_drive_path, songs,
            is_active=self.songs == self.current_tab)
        self.playlists.end_switch_drive(
            self.selected_drive_path, playlists,
            is_active=self.playlists == self.current_tab)

    def __begin_prevent_writes(self):
        self.prevent_writes = True
        self.player.disable_remove_media()

    def __end_prevent_writes(self):
        self.prevent_writes = False

    def __begin_prevent_reads(self):
        self.prevent_reads = True
        self.player.disable_controls()
        # TODO stop the player and set it to default

    def __end_prevent_reads(self):
        self.prevent_reads = False

    def async_load_drive_contents(self,
                                  missing_artist_to_fix,
                                  missing_artist_and_album_to_fix,
                                  on_complete):
        def load_songs_and_playlists(missing_artist_to_fix,
                                     missing_artist_and_album_to_fix):
            (songs_list, songs_index) = \
                load_songs(missing_artist_to_fix,
                           missing_artist_and_album_to_fix)
            playlists_list = load_playlists(songs_index)
            return (songs_list, songs_index, playlists_list)

        def load_playlists(songs_index):
            return [load_playlist(self.selected_drive_path, songs_index, path)
                    for path in self.selected_drive_path.glob("*.m3u")]

        def load_songs(missing_artist_to_fix, missing_artist_and_album_to_fix):
            def fix_bad_songs(drive_path, missing_artist, missing_artist_and_album):
                various_artists_path = drive_path / 'Various Artists'
                if missing_artist:
                    various_artists_path.mkdir(exist_ok=True)
                    for file in missing_artist:
                        dest_path = drive_path / 'Various Artists' / file.relative_to(drive_path)
                        dest_path.parent.mkdir(exist_ok=True, parents=True)
                        move_clear(file, dest_path)

                if missing_artist_and_album:
                    various_artists_path.mkdir(parents=True, exist_ok=True)
                    for file in missing_artist_and_album:
                        dest_path = drive_path / 'Various Artists' / 'Misc' / file.relative_to(drive_path)
                        dest_path.parent.mkdir(exist_ok=True, parents=True)
                        move(file, dest_path)

            fix_bad_songs(self.selected_drive_path,
                          missing_artist_to_fix,
                          missing_artist_and_album_to_fix)
            paths = self.selected_drive_path.glob("**/*/*/*.mp3")
            songs_list = list(map(song_from_semantic_filepath, paths))
            songs_index = {str(s.relative_path): s for s in songs_list}
            return (songs_list, songs_index)

        def on_load_success(songs_and_playlists):
            (songs_list, songs_index, playlists_list) = songs_and_playlists
            playlists_index = {}
            fixed_playlists = []
            for list, missing_songs in playlists_list:
                if missing_songs:
                    if self.view.ask_fix_playlist(list.name, missing_songs):
                        playlists_index[list.name] = list
                        fixed_playlists.append(list)
                else:
                    playlists_index[list.name] = list
            load_result = (songs_list, songs_index, playlists_index)

            def on_success(load_result):
                self.end_progress_floating()
                on_complete(load_result)

            def write_playlists(playlists):
                for playlist in playlists:
                    save_playlist(self.selected_drive_path, playlist)

            if fixed_playlists:
                run_async(lambda _: write_playlists(fixed_playlists),
                          lambda _: on_success(load_result),
                          self.handle_async_error)
            else:
                self.view.end_progress_floating()
                on_success(load_result)

        self.view.begin_progress_floating()
        run_async(
            lambda _: load_songs_and_playlists(missing_artist_to_fix,
                                               missing_artist_and_album_to_fix),
            on_success=on_load_success,
            on_fail=self.handle_async_error)

    def set_drive_async(self, path):
        self.view.set_selected_drive(str(path))
        self.begin_switch_drive()

        self.selected_drive_path = path

        self.player.set_selected_drive(self.selected_drive_path)
        self.view.set_status("Loading music files and playlists...")

        def bad_songs_check(_):
            drive_path = path
            missing_artist_and_album = list(drive_path.glob("*.mp3"))
            missing_artist = list(drive_path.glob("*/*.mp3"))
            return (missing_artist, missing_artist_and_album)

        def on_load_complete(load_result):
            (songs_list, songs_index, playlists_index) = load_result
            self.end_switch_drive((songs_list, songs_index), playlists_index)
            self.unlock_drive_and_backup()

            self.view.end_progress_floating()
            self.view.set_status("")

        def on_check_success(check_result):
            (missing_artist, missing_artist_and_album) = check_result
            should_fix = \
                self.view.ask_should_fix(missing_artist,
                                         missing_artist_and_album) \
                if missing_artist or missing_artist_and_album else False

            if should_fix:
                self.async_load_drive_contents(
                    missing_artist,
                    missing_artist_and_album,
                    on_load_complete)
            else:
                self.async_load_drive_contents([], [], on_load_complete)

        run_async(bad_songs_check,
                  on_success=on_check_success,
                  on_fail=self.handle_async_error)

        def on_load_success(songs_and_playlists):
            (songs_list, songs_index, playlists_list) = songs_and_playlists
            playlists_index = {}
            fixed_playlists = []
            for list, missing_songs in playlists_list:
                if missing_songs:
                    if self.view.ask_fix_playlist(list.name, missing_songs):
                        playlists_index[list.name] = list
                        fixed_playlists.append(list)
                else:
                    playlists_index[list.name] = list

            def on_load_complete(_):
                self.end_switch_drive((songs_list, songs_index), playlists_index)
                self.unlock_drive_and_backup()

                self.view.end_progress_floating()
                self.view.set_status("")

            def write_playlists(playlists):
                for playlist in playlists:
                    save_playlist(self.selected_drive_path, playlist)

            if fixed_playlists:
                run_async(lambda _: write_playlists(fixed_playlists),
                          on_load_complete,
                          self.handle_async_error)
            else:
                on_load_complete(None)

    def on_select_backup(self):
        backup_path_string = self.view.choose_backup_location_dialog()

        if backup_path_string:
            self.selected_backup_path = Path(backup_path_string)
            self.view.set_backup_location(backup_path_string)
            self.enable_backup_actions()

    def on_forget_backup(self):
        self.disable_backup_actions()
        self.view.set_backup_location('')
        self.selected_backup_path = None

    def disable_backup_actions(self):
        self.view.disable_backup()
        self.view.disable_restore()
        self.view.disable_forget()

    def prepare_remove_song(self, song):
        self.playlists.prepare_remove_song(song)

    def enable_backup_actions(self):
        if self.selected_backup_path:
            self.view.enable_backup()
            self.view.enable_restore()
            self.view.enable_forget()

    def lock_drive_and_backup(self):
        self.view.disable_drive_select()
        self.view.disable_backup_select()
        self.view.disable_forget()

    def unlock_drive_and_backup(self):
        self.view.enable_drive_select()
        self.view.enable_backup_select()
        if self.selected_backup_path:
            self.view.enable_forget()

    def lock_drive(self):
        self.view.disable_drive_select()
        self.view.disable_restore()
        self.view.disable_backup()

    def unlock_drive(self):
        self.view.enable_drive_select()
        if self.selected_backup_path:
            self.view.enable_restore()
            self.view.enable_backup()

    def set_status(self, status):
        self.view.set_status(status)

    def begin_progress(self, n):
        self.view.begin_progress(n)

    def begin_progress_floating(self):
        self.view.begin_progress_floating()

    def end_progress_floating(self):
        self.view.end_progress_floating()

    def end_progress(self):
        self.view.end_progress()

    def report_progress(self, value):
        self.view.report_progress(value)

    def on_switch_tab(self, n):
        if n == 0:
            self.current_tab = self.songs
        elif n == 1:
            self.current_tab = self.playlists

        self.current_tab.focus()
        self.player.subscribe(self.current_tab)

    def choose_metadata_dialog(self, from_id3, from_path):
        dialog = self.view.choose_metadata_dialog()
        controller = MetadataController(dialog, from_id3, from_path)
        return controller.metadata() if dialog.exec() else None
