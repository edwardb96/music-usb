from song import SongMetadata


class MetadataController(object):
    def __init__(self, view, from_id3, from_path):
        self.view = view
        self.view.subscribe(self)
        self.from_id3 = from_id3
        self.from_path = from_path
        self._metadata = None
        self.apply_preset(self.from_path)

    def apply_preset(self, p):
        self.view.set_artist(p.artist)
        self.view.set_album_artist(p.album_artist)
        self.view.set_album(p.album)
        self.view.set_track_title(p.track_title)

    def metadata_from_ui(self):
        def empty_to_none(s):
            return s if not is_empty(s) else None

        def is_empty(s):
            return s == '' or s.isspace()

        artist = empty_to_none(self.view.artist())
        album_artist = empty_to_none(self.view.album_artist())
        album = self.view.album()
        track_title = self.view.track_title()

        if (artist or album_artist) and \
                not (is_empty(album) or is_empty(track_title)):
            return SongMetadata(artist, album_artist, album, track_title)
        else:
            return None

    def update_ok(self):
        if self._metadata:
            self.view.enable_ok()
        else:
            self.view.disable_ok()

    def on_artist_changed(self, new_artist):
        self._metadata = self.metadata_from_ui()
        self.update_ok()

    def on_album_changed(self, new_album):
        self._metadata = self.metadata_from_ui()
        self.update_ok()

    def on_album_artist_changed(self, new_album_artist):
        self._metadata = self.metadata_from_ui()
        self.update_ok()

    def on_title_changed(self, new_title):
        self._metadata = self.metadata_from_ui()
        self.update_ok()

    def on_preset_selected(self, index):
        if index == 0:
            self.apply_preset(self.from_path)
        elif index == 1:
            self.apply_preset(self.from_id3)

    def metadata(self):
        return self._metadata
