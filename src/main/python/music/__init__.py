from .metadata import id3_metadata, path_metadata, compilation_path_metadata
from song import Song

def try_song_from_library(path):
    from_id3 = id3_metadata(str(path))
    from_path = path_metadata(path)

    if from_id3:
        if from_id3.can_merge(from_path):
            return Song(metadata=from_id3.merge(from_path))
        else:
            if from_id3.album_artist == 'Various Artists':
                from_compilation_path = compilation_path_metadata(path)
                if from_id3.can_merge(from_compilation_path):
                    return Song(from_id3.merge(from_compilation_path))
            return (from_id3, from_path)
            # NOTE: if there are lots of these, then we should probably
            # have the dialog remember which options we picked.
    else:
        return Song(from_path)
