from mutagen import File
from mutagen.easyid3 import EasyID3
from mutagen.id3 import ID3NoHeaderError
from song import SongMetadata, Song


def id3_artist(filepath):
    try:
        metadata = EasyID3(filepath)
        return metadata['artist'][0] if 'artist' in metadata else None
    except ID3NoHeaderError:
        return None


def try_album_art(filepath):
    try:
        metadata = File(filepath)
        for tag in metadata.tags.values():
            if tag.FrameID == 'APIC':
                return tag.data
        return None
    except ID3NoHeaderError:
        return None


def id3_metadata(filepath):
    try:
        metadata = EasyID3(filepath)

        artist = metadata['artist'][0] if 'artist' in metadata else None
        album_artist = metadata['albumartist'][0] \
            if 'albumartist' in metadata else None
        album = metadata['album'][0] if 'album' in metadata else None
        track_title = metadata['title'][0] if 'title' in metadata else None

        if album and track_title:
            # A valid song metadata must have an artist or an album_artist
            # it can have both but not neither.
            if artist or album_artist:
                return SongMetadata(artist, album_artist, album, track_title)

        # ID3 missing required key
        return None
    except ID3NoHeaderError:
	    return None


def path_metadata(path):
    album_directory = path.parent
    artist_directory = album_directory.parent
    assert artist_directory != artist_directory.root
    return SongMetadata(track_title=path.stem,
                        artist=None,
                        album_artist=artist_directory.name,
                        album=album_directory.name)


def compilation_path_metadata(path):
    album_directory = path.parent
    return SongMetadata(track_title=path.stem,
                        artist=None,
                        album_artist='Various Artists',
                        album=album_directory.name)


def song_from_semantic_filepath(path):
    assert(path.is_file())
    metadata = path_metadata(path)
    metadata.artist = id3_artist(str(path))
    return Song(metadata)
