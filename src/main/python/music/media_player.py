import os
import sys

from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from PyQt5.QtCore import QUrl
from os.path import abspath 

#if sys.platform.startswith('win'):
#	os.environ['PYTHON_VLC_MODULE_PATH'] = abspath('libvlc.dll')

from vlc import Instance, EventType


class VlcMp3Player(object):
    def __init__(self, ui, on_state_changed_callback):

        self.instance = Instance()
        self.player = self.instance.media_player_new()
        self.instance.player = self.player
        self.on_state_changed_callback = on_state_changed_callback
        self.player.audio_set_volume(50)
        event_manager = self.player.event_manager()
        event_manager.event_attach(EventType.MediaPlayerEndReached, self.on_stopped)
        event_manager.event_attach(EventType.MediaPlayerPaused, self.on_paused)
        event_manager.event_attach(EventType.MediaPlayerPlaying, self.on_playing)

    def on_paused(self, evt):
        self.on_state_changed_callback(False)

    def on_playing(self, evt):
        self.on_state_changed_callback(True)

    def on_stopped(self, evt):
        self.on_state_changed_callback(False)

    def on_play_pause_end(self):
        self.on_state_changed_callback(self.player.is_playing())

    def set_media(self, path):
        media = self.instance.media_new(path)
        self.player.set_media(media)

    def is_playing(self):
        return self.player.is_playing()

    def play(self):
        self.player.play()

    def pause(self):
        self.player.pause()


class QtMp3Player(object):
    def __init__(self, ui, on_state_changed_callback):
        self.player = QMediaPlayer(ui.main_window)
        self.player.setVolume(50)
        self.on_state_changed_callback = on_state_changed_callback
        self.player.stateChanged.connect(self.__on_state_changed)

    def set_media(self, path):
        url = QUrl.fromLocalFile(path)
        self.player.setMedia(QMediaContent(url))

    def is_playing(self):
        return is_playing(self.player.state())

    def play(self):
        self.player.play()

    def pause(self):
        self.player.pause()

    def __on_state_changed(self, state):
        self.on_state_changed_callback(is_playing(state))


def is_playing(state):
    return state == QMediaPlayer.State.PlayingState
