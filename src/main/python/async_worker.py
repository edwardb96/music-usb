from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject, QThreadPool, QRunnable

import traceback
import sys


def run_async(fn,
              on_success=lambda _: None,
              on_fail=lambda _: None,
              on_progress=lambda _: None):
    worker = Worker(fn)
    worker.signals.result.connect(on_success)
    worker.signals.error.connect(on_fail)
    worker.signals.progress.connect(on_progress)
    QThreadPool.globalInstance().start(worker)


class WorkerSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)


class Worker(QRunnable):
    def __init__(self, action):
        super(Worker, self).__init__()
        self.action = action
        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        try:
            result = self.action(self.signals.progress.emit)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)
