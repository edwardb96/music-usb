from fbs_runtime.application_context.PyQt5 import ApplicationContext
from ui import UI
from controller import Controller
from sys import exit, argv
from datetime import datetime
from pathlib import Path
from os.path import isdir, abspath

import traceback

if __name__ == '__main__':
    try:
        ctx = ApplicationContext()       # 1. Instantiate ApplicationContext
        paths = {
            'pause': ctx.get_resource('icons/pause.svg'),
            'play': ctx.get_resource('icons/play.svg'),
            'next': ctx.get_resource('icons/next.svg'),
            'prev': ctx.get_resource('icons/prev.svg'),
            'trash': ctx.get_resource('icons/trash.svg'),
            'default_album_art': ctx.get_resource('icons/default-album-art.svg'),
            'add-file': ctx.get_resource('icons/add-file.svg'),
            'add-dir': ctx.get_resource('icons/add-directory.svg'),
            'backup': ctx.get_resource('icons/backup.svg'),
            'restore': ctx.get_resource('icons/restore.svg')
        }
        ui = UI(paths)
        controller = Controller(ui)
        if len(argv) > 1 and isdir(argv[1]):
            controller.set_drive_async(Path(abspath(argv[1])))
        ui.show()
        exit_code = ctx.app.exec_()      # 2. Invoke ctx.app.exec_()
        exit(exit_code)
    except Exception as ex:
        with open("crash.log", "a+") as error_log:
            # print exception
            traceback.print_exc()
            # log exception
            traceback_exc = traceback.format_exc()
            date_time_string = datetime.now().strftime("%I:%M%p on %B %d, %Y")
            error_log.write("## error on gui - {}\n".format(date_time_string))
            error_log.write(traceback_exc + "\n")
        exit(1)
