from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, \
                            QPushButton, QLabel, QSizePolicy, QComboBox, \
                            QLineEdit, QLayout
from PyQt5.QtCore import QSize


class MetadataDialog(object):
    def __init__(self, parent):
        self.dialog = QDialog(parent)
        self.dialog.setModal(True)
        self.dialog.setWindowTitle("Enter Track Metadata")

        vertical_layout = QVBoxLayout(self.dialog)
        vertical_layout.setContentsMargins(11, 11, 11, 11)
        vertical_layout.setSpacing(6)
        self.dialog.layout().setSizeConstraint(QLayout.SetFixedSize)

        self.preset_combobox = QComboBox(self.dialog)
        self.preset_combobox.addItems(["From Directory Structure",
                                       "From MP3 Metadata"])
        vertical_layout.addWidget(self.preset_combobox)

        def make_field(field_name):
            field_layout = QHBoxLayout()
            field_layout.setSpacing(6)

            field_label = QLabel("{}:".format(field_name), self.dialog)
            field_label.setMinimumSize(QSize(150, 0))
            field_label.setSizePolicy(QSizePolicy(QSizePolicy.Preferred,
                                                  QSizePolicy.Preferred))
            field_layout.addWidget(field_label)

            field_edit = QLineEdit()
            field_edit.setMinimumSize(QSize(300, 0))

            field_layout.addWidget(field_edit)
            return (field_layout, field_edit)

        (title_layout, title_edit) = make_field("Title")
        self.title_edit = title_edit
        vertical_layout.addLayout(title_layout)

        (album_layout, album_edit) = make_field("Album")
        self.album_edit = album_edit
        vertical_layout.addLayout(album_layout)

        (album_artist_layout, album_artist_edit) = make_field("Album Artist")
        self.album_artist_edit = album_artist_edit
        vertical_layout.addLayout(album_artist_layout)

        (artist_layout, artist_edit) = make_field("Artist")
        self.artist_edit = artist_edit
        vertical_layout.addLayout(artist_layout)

        def make_action_layout():
            action_layout = QHBoxLayout()
            self.skip_button = QPushButton("Skip File", self.dialog)
            self.skip_button.clicked.connect(self.dialog.reject)
            action_layout.addWidget(self.skip_button)

            self.ok_button = QPushButton("OK", self.dialog)
            self.ok_button.setEnabled(False)
            self.ok_button.clicked.connect(self.dialog.accept)
            action_layout.addWidget(self.ok_button)
            return action_layout

        action_layout = make_action_layout()
        vertical_layout.addLayout(action_layout)

    def subscribe(self, controller):
        self.artist_edit.textChanged.connect(controller.on_artist_changed)
        self.album_artist_edit.textChanged.connect(
            controller.on_album_artist_changed)
        self.album_edit.textChanged.connect(controller.on_album_changed)
        self.title_edit.textChanged.connect(controller.on_title_changed)
        self.preset_combobox.currentIndexChanged.connect(
            controller.on_preset_selected)

    def accept(self):
        self.dialog.accept()

    def enable_ok(self):
        self.ok_button.setEnabled(True)

    def disable_ok(self):
        self.ok_button.setEnabled(False)

    def track_title(self):
        return self.title_edit.text()

    def set_track_title(self, new_title):
        self.title_edit.setText(new_title)

    def artist(self):
        return self.artist_edit.text()

    def set_artist(self, new_artist):
        self.artist_edit.setText(new_artist)

    def album(self):
        return self.album_edit.text()

    def set_album(self, new_album):
        self.album_edit.setText(new_album)

    def album_artist(self):
        return self.album_artist_edit.text()

    def set_album_artist(self, new_album_artist):
        self.album_artist_edit.setText(new_album_artist)

    def exec(self):
        return self.dialog.exec() == QDialog.DialogCode.Accepted
