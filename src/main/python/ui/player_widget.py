from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, \
                            QPushButton, QLabel, QSizePolicy, \
                            QFrame, QGroupBox
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QIcon, QPixmap


album_art_size = QSize(185, 185)


class PlayerWidget(QGroupBox):
    def __init__(self, icons):
        super().__init__("Player")
        player_layout = QHBoxLayout(self)
        player_layout.setSpacing(12)
        self.icons = icons
        self.album_art_label = QLabel("", self)
        self.album_art_label.setPixmap(
            self.icons['default_album_art'].pixmap(album_art_size))
        self.album_art_label.setMinimumSize(album_art_size)
        self.album_art_label.setMaximumSize(album_art_size)
        self.album_art_label.setLineWidth(1)
        self.album_art_label.setFrameStyle(QFrame.Box)
        player_layout.addWidget(self.album_art_label)

        controls_info_layout = QVBoxLayout()
        controls_info_layout.setSpacing(6)
        size_policy = QSizePolicy(QSizePolicy.Expanding,
                                  QSizePolicy.Preferred)
        self.track_name_label = QLabel("Track Name")
        self.track_name_label.setSizePolicy(size_policy)
        controls_info_layout.addWidget(self.track_name_label)
        self.album_label = QLabel("Album")
        self.album_label.setSizePolicy(size_policy)
        controls_info_layout.addWidget(self.album_label)
        self.artist_label = QLabel("Artist")
        self.artist_label.setSizePolicy(size_policy)
        controls_info_layout.addWidget(self.artist_label)

        button_layout = QHBoxLayout()
        button_layout.setSpacing(6)

        def make_icon_button(icon_name):
            button = QPushButton(self.icons[icon_name], "", self)
            button.setMinimumSize(QSize(50, 50))
            button.setIconSize(QSize(40, 40))
            button.setEnabled(False)
            return button

        def add_icon_button(icon_name):
            button = make_icon_button(icon_name)
            button_layout.addWidget(button)
            return button

        self.previous_button = add_icon_button('prev')
        self.play_pause_button = add_icon_button('play')
        self.next_button = add_icon_button('next')
        controls_info_layout.addLayout(button_layout)

        self.remove_button = make_icon_button('trash')
        controls_info_layout.addWidget(self.remove_button)

        player_layout.addLayout(controls_info_layout)

    def subscribe(self, controller):
        self.play_pause_button.clicked.connect(controller.on_play_pause)
        self.next_button.clicked.connect(controller.on_next)
        self.previous_button.clicked.connect(controller.on_previous)
        self.remove_button.clicked.connect(controller.on_remove)

    def enable_remove_media(self):
        self.remove_button.setEnabled(True)

    def disable_remove_media(self):
        self.remove_button.setEnabled(False)

    def disable_controls(self):
        self.play_pause_button.setEnabled(False)
        self.next_button.setEnabled(False)
        self.previous_button.setEnabled(False)

    def enable_controls(self):
        self.play_pause_button.setEnabled(True)
        self.next_button.setEnabled(True)
        self.previous_button.setEnabled(True)

    def set_track_title(self, track_title):
        self.track_name_label.setText(track_title)

    def set_album(self, album):
        self.album_label.setText(album)

    def set_artist(self, artist):
        self.artist_label.setText(artist)

    def set_album_art(self, apic):
        pixmap = QPixmap()
        pixmap.loadFromData(apic)
        scaled_pixmap = pixmap.scaled(album_art_size,
                                      Qt.IgnoreAspectRatio,
                                      Qt.SmoothTransformation)
        self.album_art_label.setPixmap(scaled_pixmap)

    def reset_album_art(self):
        self.album_art_label.setPixmap(
            self.icons['default_album_art'].pixmap(album_art_size))

    def show_pause_button(self):
        self.play_pause_button.setIcon(self.icons["pause"])

    def show_play_button(self):
        self.play_pause_button.setIcon(self.icons["play"])
