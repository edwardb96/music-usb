from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QPushButton, \
                            QLabel, QLayout, QTextEdit
from PyQt5.QtCore import QSize


class FixPlaylistDialog(object):
    def __init__(self, parent, playlist_name, missing_songs):
        self.dialog = QDialog(parent)
        self.dialog.setModal(True)
        self.dialog.setWindowTitle("Fix Broken References?")

        vertical_layout = QVBoxLayout(self.dialog)
        vertical_layout.setContentsMargins(11, 11, 11, 11)
        vertical_layout.setSpacing(6)
        self.dialog.layout().setSizeConstraint(QLayout.SetFixedSize)

        question = "Expected all files referred to by the playlist '{}' to be present on the drive, however the following file(s) are missing\n\nWould you like to fix the playlist by removing references to the missing songs or ignore this playlist?"
        self.question_label = QLabel(question.format(playlist_name))
        self.question_label.setWordWrap(True)
        self.question_label.setMinimumSize(QSize(750, 10))
        vertical_layout.addWidget(self.question_label)

        self.bad_files_text = QTextEdit(self.dialog)
        self.bad_files_text.setReadOnly(True)
        message = '\n'.join(missing_songs)
        self.bad_files_text.setLineWrapMode(QTextEdit.LineWrapMode.NoWrap)
        self.bad_files_text.setText(message)
        vertical_layout.addWidget(self.bad_files_text)

        def make_action_layout():
            action_layout = QHBoxLayout()
            self.ignore_button = QPushButton("Ignore", self.dialog)
            self.ignore_button.clicked.connect(self.dialog.reject)
            action_layout.addWidget(self.ignore_button)

            self.fix_button = QPushButton("Fix", self.dialog)
            self.fix_button.clicked.connect(self.dialog.accept)
            action_layout.addWidget(self.fix_button)
            return action_layout

        action_layout = make_action_layout()
        vertical_layout.addLayout(action_layout)

    def exec(self):
        return self.dialog.exec() == QDialog.DialogCode.Accepted
