from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, \
                            QTreeView, QAbstractItemView, QWidget, \
                            QListWidget, QInputDialog, QMessageBox
from PyQt5.QtCore import Qt, QSize, QSignalBlocker
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from song import Song


class PlaylistsWidget(QWidget):
    def __init__(self, parent, icons):
        super().__init__(parent)
        self.icons = icons
        main_layout = QHBoxLayout(self)

        def make_playlists_layout(ui):
            layout = QVBoxLayout()

            ui.playlists = QListWidget()
            ui.playlists.setEditTriggers(QAbstractItemView.NoEditTriggers)
            ui.playlists.currentItemChanged.connect(self.on_playlist_changed)
            layout.addWidget(self.playlists)

            ui.new_playlist_button = QPushButton("New Playlist", self)
            ui.new_playlist_button.setMinimumSize(QSize(50, 50))
            ui.new_playlist_button.setEnabled(False)
            layout.addWidget(ui.new_playlist_button)

            ui.remove_button = QPushButton(self.icons["trash"], "", self)
            ui.remove_button.setMaximumSize(QSize(16777215, 16777215))
            ui.remove_button.setMinimumSize(QSize(50, 50))
            ui.remove_button.setIconSize(QSize(40, 40))
            ui.remove_button.setEnabled(False)
            layout.addWidget(ui.remove_button)
            return layout

        main_layout.addLayout(make_playlists_layout(self))
        main_layout.setStretch(0, 1)

        self.playlist = QTreeView()
        self.playlist.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.playlist_model = QStandardItemModel()
        self.playlist_model.rowsInserted.connect(self.on_rows_inserted)
        self.playlist_model.rowsRemoved.connect(self.on_rows_removed)
        self.playlist_model.setHorizontalHeaderLabels(["Track",
                                                       "Album",
                                                       "Artist",
                                                       "Album Artist",
                                                       "Path"])

        self.playlist.setModel(self.playlist_model)
        self.playlist.setMinimumSize(QSize(700, 500))
        self.playlist.setSelectionMode(
            QAbstractItemView.SelectionMode.SingleSelection)
        self.playlist.setDragEnabled(True)  # need to get signals
        self.playlist.viewport().setAcceptDrops(True)
        self.playlist.setDropIndicatorShown(True)
        self.playlist.setDragDropMode(
            QAbstractItemView.DragDropMode.InternalMove)
        self.playlist.selectionModel().currentChanged \
                                      .connect(self.on_song_changed)

        self.swap_from = None
        self.item_changed_callback = None
        self.playlist_changed_callback = None

        main_layout.addWidget(self.playlist)
        main_layout.setStretch(1, 5)

    def disable_reorder(self):
        self.playlist.setDragEnabled(False)
        self.playlist.viewport().setAcceptDrops(False)
        self.playlist.setDropIndicatorShown(False)

    def enable_reorder(self):
        self.playlist.setDragEnabled(True)
        self.playlist.viewport().setAcceptDrops(True)
        self.playlist.setDropIndicatorShown(True)

    def enable_songs_list(self):
        self.playlist.setEnabled(True)

    def disable_songs_list(self):
        self.playlist.setEnabled(False)

    def enable_playlist_list(self):
        self.playlists.setEnabled(True)

    def disable_playlist_list(self):
        self.playlists.setEnabled(False)

    def add_playlist(self, name):
        self.playlists.addItem(name)

    def clear_playlists(self):
        self.playlists.clear()

    def ask_playlist_name(self):
        (name, ok) = QInputDialog.getText(self,
                                          "Enter Playlist Name",
                                          "Playlist Name")
        name_stripped = name.strip()
        if ok and name_stripped:
            return name_stripped
        else:
            return None

    def enable_remove(self):
        self.remove_button.setEnabled(True)

    def disable_remove(self):
        self.remove_button.setEnabled(False)

    def on_rows_inserted(self, p, s, e):
        assert s == e
        self.swap_to = s
        # insert happens then removal happens

    def remove_selected_song(self):
        remove_at = self.playlist.currentIndex().row()
        if self.playlist_model.rowCount() == 1:
            self.clear_selection()
        else:
            self.select_next(before_removal=True)
        self.playlist_model.removeRows(remove_at, 1)

    def remove_at(self, n):
        current_row = self.playlist.currentIndex().row()
        if self.playlist_model.rowCount() == 1:
            self.clear_selection()
        elif current_row == n:
            self.select_next(before_removal=True)
        self.playlist_model.removeRows(n, 1)

    def select_first_song(self):
        begining = self.playlist_model.index(0, 0)
        self.set_current_index(begining)

    def select_next(self, before_removal=False):
        current = self.playlist.currentIndex()
        next = self.playlist.indexBelow(current)
        if next.isValid():
            self.set_current_index(next, silent=before_removal)
        else:
            begining = self.playlist_model.index(0, 0)
            self.set_current_index(begining, silent=False)

    def select_previous(self):
        current = self.playlist.currentIndex()
        next = self.playlist.indexAbove(current)
        if next.isValid():
            self.set_current_index(next)
        else:
            end = self.playlist_model.index(
                      self.playlist_model.rowCount() - 1, 0)
            self.set_current_index(end)

    def clear_selection(self):
        self.playlist.clearSelection()

    def set_current_index(self, idx, silent=False):
        if silent:
            with QSignalBlocker(self.playlist.selectionModel()):
                self.playlist.setCurrentIndex(idx)
        else:
            self.playlist.setCurrentIndex(idx)

    def on_rows_removed(self, p, s, e):
        if self.swap_to is not None:
            assert s == e
            if self.swap_to < s:
                self.swap_callback(s-1, self.swap_to)
            else:
                self.swap_callback(s, self.swap_to)
            self.swap_to = None

    def add_song(self, song):
        self.playlist_model.appendRow(make_row(song))
        self.swap_to = None

    def clear_songs(self):
        self.playlist_model.removeRows(0, self.playlist_model.rowCount())

    def add_songs(self, playlist):
        for song in playlist:
            self.playlist_model.appendRow(make_row(song))
        self.swap_to = None

        for c in range(Song.COLUMN_COUNT):
            self.playlist.resizeColumnToContents(c)

    def set_songs(self, playlist):
        self.clear_songs()
        self.add_songs(playlist)

    def on_playlist_changed(self, current_item, previous_item):
        if current_item is None:
            self.on_playlist_changed_callback(None)
        else:
            self.on_playlist_changed_callback(current_item.text())

    def on_song_changed(self, current_idx, previous_idx):
        if current_idx.isValid():
            self.item_changed_callback(current_idx.row())
        else:
            self.item_changed_callback(None)

    def ask_remove_playlist(self, playlist_name):
        message = \
            "This will delete the playlist '{}'.\n\nDo you want to continue?" \
                .format(playlist_name)
        yes_no = QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No
        result = \
            QMessageBox.warning(self, "Delete Playlist?", message,
                                buttons=yes_no,
                                defaultButton=QMessageBox.StandardButton.No)
        return result == QMessageBox.StandardButton.Yes

    def remove_selected_playlist(self):
        index = self.playlists.selectedIndexes()[0].row()
        self.playlists.model().removeRow(index)
        self.playlists.clearSelection()

    def subscribe(self, controller):
        self.item_changed_callback = controller.on_selected_song_changed
        self.swap_callback = controller.on_reorder
        self.remove_button.clicked.connect(controller.on_remove_playlist)
        self.new_playlist_button.clicked.connect(controller.on_new_playlist)
        self.on_playlist_changed_callback = \
            controller.on_selected_playlist_changed

    def enable_new_playlist(self):
        self.new_playlist_button.setEnabled(True)

    def disable_new_playlist(self):
        self.new_playlist_button.setEnabled(False)


def make_row(song):
    return [make_item(t) for t in song.to_row()]


def make_item(t):
    flags = Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsDragEnabled
    item = QStandardItem(t)
    item.setFlags(flags)
    return item
