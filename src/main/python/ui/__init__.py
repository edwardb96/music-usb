from PyQt5.QtWidgets import QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, \
                            QPushButton, QLabel, QSizePolicy, QProgressBar, \
                            QStatusBar, QFileDialog, QGroupBox, QTabWidget
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize
from .metadata_dialog import MetadataDialog
from .fix_files_dialog import FixFilesDialog
from .fix_playlist_dialog import FixPlaylistDialog
from .playlists_widget import PlaylistsWidget
from .songs_widget import SongsWidget
from .player_widget import PlayerWidget


class UI(object):
    def __init__(self, paths):
        self.main_window = QMainWindow()
        self.main_window.resize(900, 750)
        self.main_window.setWindowTitle("Music USB")
        self.icons = {k: QIcon(p) for (k, p) in paths.items()}

        central_widget = QWidget(self.main_window)
        self.main_window.setCentralWidget(central_widget)

        vertical_layout = QVBoxLayout(central_widget)
        vertical_layout.setContentsMargins(11, 11, 11, 11)
        vertical_layout.setSpacing(6)

        def make_drive_group(ui):
            drive_group = QGroupBox("Drive", central_widget)
            drive_layout = QVBoxLayout(drive_group)
            drive_layout.setSpacing(6)

            def add_path_selection_layout(button_text):
                # Drive Path
                layout = QHBoxLayout()
                layout.setSpacing(6)

                label = QLabel(drive_group)
                label.setSizePolicy(QSizePolicy(QSizePolicy.Expanding,
                                                QSizePolicy.Preferred))
                layout.addWidget(label)

                button = QPushButton(button_text, drive_group)
                button.setMinimumSize(QSize(150, 0))
                layout.addWidget(button)

                drive_layout.addLayout(layout)
                return (label, button)

            (ui.drive_label, ui.select_drive_button) = \
                add_path_selection_layout('Select Drive')
            (ui.backup_label, ui.select_backup_button) = \
                add_path_selection_layout('Select Backup')
            ui.select_backup_button.setEnabled(False)

            drive_layout.addStretch(0)

            sync_restore_layout = QHBoxLayout()

            def add_icon_button(text, icon_name=None):
                button = QPushButton(text, drive_group)
                button.setMinimumSize(QSize(50, 50))
                button.setEnabled(False)
                if icon_name:
                    button.setIcon(ui.icons[icon_name])
                    button.setIconSize(QSize(40, 40))
                sync_restore_layout.addWidget(button)
                return button

            ui.backup_button = add_icon_button("Backup", "backup")
            ui.restore_button = add_icon_button("Restore", "restore")
            ui.forget_button = add_icon_button("Forget")
            drive_layout.addLayout(sync_restore_layout)

            return drive_group

        top_layout = QHBoxLayout()
        self.player = PlayerWidget(self.icons)
        self.player.setSizePolicy(QSizePolicy(QSizePolicy.Expanding,
                                              QSizePolicy.Fixed))
        top_layout.addWidget(self.player)
        drive_group = make_drive_group(self)
        drive_group.setFixedHeight(self.player.sizeHint().height()-6)
        top_layout.addWidget(drive_group)

        vertical_layout.addLayout(top_layout)

        self.tab_widget = QTabWidget()
        vertical_layout.addWidget(self.tab_widget)

        self.songs = SongsWidget(self.main_window, self.icons)
        self.tab_widget.addTab(self.songs, "Songs")

        self.playlists = PlaylistsWidget(self.main_window, self.icons)
        self.tab_widget.addTab(self.playlists, "Playlists")

        self.progress_bar = QProgressBar()
        self.progress_bar.setEnabled(False)

        vertical_layout.addWidget(self.progress_bar)

        self.status_bar = QStatusBar(self.main_window)
        self.main_window.setStatusBar(self.status_bar)
        self.status_bar.showMessage("Select the music drive")

    def disable_backup(self):
        self.backup_button.setEnabled(False)

    def disable_restore(self):
        self.restore_button.setEnabled(False)

    def disable_forget(self):
        self.forget_button.setEnabled(False)

    def enable_backup(self):
        self.backup_button.setEnabled(True)

    def enable_restore(self):
        self.restore_button.setEnabled(True)

    def enable_forget(self):
        self.forget_button.setEnabled(True)

    def show(self):
        self.main_window.showMaximized()

    def item_count(self):
        return self.songs_model.rowCount()

    def set_status(self, message):
        self.status_bar.showMessage(message)

    def set_selected_drive(self, path):
        self.drive_label.setText(path)

    def enable_drive_select(self):
        self.select_drive_button.setEnabled(True)

    def disable_drive_select(self):
        self.select_drive_button.setEnabled(False)

    def enable_backup_select(self):
        self.select_backup_button.setEnabled(True)

    def disable_backup_select(self):
        self.select_backup_button.setEnabled(False)

    def set_backup_location(self, path):
        self.backup_label.setText(path)

    def subscribe(self, controller):
        # Folder selection
        self.select_drive_button.clicked.connect(controller.on_select_drive)
        self.select_backup_button.clicked.connect(controller.on_select_backup)

        # Backup Buttons
        self.backup_button.clicked.connect(controller.on_backup)
        self.forget_button.clicked.connect(controller.on_forget_backup)
        self.restore_button.clicked.connect(controller.on_restore)

        # Tab Switching
        self.tab_widget.currentChanged.connect(controller.on_switch_tab)

    def ask_should_fix(self, missing_artist, missing_artist_and_album):
        dialog = FixFilesDialog(self.main_window,
                                missing_artist,
                                missing_artist_and_album)
        return dialog.exec()

    def ask_fix_playlist(self, playlist_name, missing_songs):
        dialog = FixPlaylistDialog(self.main_window,
                                   playlist_name,
                                   missing_songs)
        return dialog.exec()

    def choose_drive_dialog(self):
        return QFileDialog.getExistingDirectory(self.main_window,
                                                "Select Drive Directory")

    def choose_backup_location_dialog(self):
        return QFileDialog.getExistingDirectory(self.main_window,
                                                "Select Backup Directory")


    def begin_progress_floating(self):
        self.progress_bar.setEnabled(True)
        self.progress_bar.setValue(0)
        self.progress_bar.setRange(0, 0)

    def end_progress_floating(self):
        self.progress_bar.setRange(0, 100)
        self.progress_bar.setValue(0)
        self.progress_bar.setEnabled(False)

    def disable_player_controls(self):
        self.play_pause_button.setEnabled(False)
        self.next_button.setEnabled(False)
        self.previous_button.setEnabled(False)

    def enable_player_controls(self):
        self.play_pause_button.setEnabled(True)
        self.next_button.setEnabled(True)
        self.previous_button.setEnabled(True)

    def begin_progress(self, end):
        assert(end > 0)
        self.progress_bar.setEnabled(True)
        self.progress_bar.setRange(0, end)
        self.progress_bar.setValue(0)

    def report_progress(self, value):
        self.progress_bar.setValue(value)

    def end_progress(self):
        self.progress_bar.setValue(0)
        self.progress_bar.setEnabled(False)

    def choose_metadata_dialog(self):
        return MetadataDialog(self.main_window)
