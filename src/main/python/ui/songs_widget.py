from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, QTreeView, \
                            QLineEdit, QWidget, QMenu, QFileDialog, \
                            QAbstractItemView, QMessageBox
from PyQt5.QtCore import Qt, QSize, QPoint, QSortFilterProxyModel, \
                         QSignalBlocker, QStandardPaths
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from song import Song

music_location = QStandardPaths.standardLocations(
                    QStandardPaths.StandardLocation.MusicLocation)[0]


class SongsWidget(QWidget):
    def __init__(self, main_window, icons):
        super().__init__()
        self.main_window = main_window
        self.icons = icons

        self.can_add_to_playlist = True
        self.context_menu = QMenu()
        self.context_menu_actions = []

        songs_layout = QVBoxLayout(self)

        def make_search_layout(ui):
            search_layout = QHBoxLayout()
            search_layout.setSpacing(6)
            ui.filter_edit = QLineEdit(self)
            search_layout.addWidget(ui.filter_edit)

            ui.clear_search_button = QPushButton("Clear Search", self)
            ui.clear_search_button.setMinimumSize(QSize(150, 0))
            ui.clear_search_button.setEnabled(False)
            search_layout.addWidget(ui.clear_search_button)

            return search_layout

        songs_layout.addLayout(make_search_layout(self))

        self.songs = QTreeView(self)
        self.songs.setEditTriggers(QAbstractItemView.NoEditTriggers)

        self.songs_model = QStandardItemModel(0, 5)
        self.songs_model.setHorizontalHeaderLabels(["Track",
                                                    "Album",
                                                    "Artist",
                                                    "Album Artist",
                                                    "Path"])

        self.songs.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
        self.songs.setSortingEnabled(True)

        self.songs.sortByColumn(0, Qt.SortOrder.AscendingOrder)
        self.sort_model = QSortFilterProxyModel()
        self.sort_model.setFilterKeyColumn(-1)
        self.sort_model.setSourceModel(self.songs_model)

        self.songs.setModel(self.sort_model)
        self.songs.selectionModel().currentChanged.connect(self.item_changed)
        self.songs.customContextMenuRequested \
            .connect(self.custom_context_menu_requested)

        self.song_item_background_brush = QStandardItem("").background()
        self.item_changed_callback = None

        songs_layout.addWidget(self.songs)

        def make_extra_button_layout(ui):
            button_horizontal_layout = QHBoxLayout()
            button_horizontal_layout.setSpacing(6)

            def add_icon_button(text, icon_name):
                button = QPushButton(text, self)
                button.setMinimumSize(QSize(50, 50))
                button.setEnabled(False)
                button.setIcon(self.icons[icon_name])
                button.setIconSize(QSize(40, 40))
                button_horizontal_layout.addWidget(button)
                return button

            # Add Buttons
            ui.add_files_button = \
                add_icon_button("Add Music Files", "add-file")
            ui.add_directory_button = \
                add_icon_button("Add Music Directory", "add-dir")

            return button_horizontal_layout

        songs_layout.addLayout(make_extra_button_layout(self))

    def item_changed(self, current_idx, previous_idx):
        if current_idx.isValid():
            item_index = self.sort_model.mapToSource(current_idx) \
                                        .siblingAtColumn(4)
            item = self.songs_model.itemFromIndex(item_index)
            self.item_changed_callback(item.text())
        else:
            self.item_changed_callback(None)

    def current_index(self):
        row = self.songs.currentIndex().row()
        return row if row >= 0 else None

    def clear_selection(self):
        self.songs.clearSelection()

    def set_current_index(self, idx, silent=False):
        if silent:
            with QSignalBlocker(self.songs.selectionModel()):
                self.songs.setCurrentIndex(idx)
        else:
            self.songs.setCurrentIndex(idx)

    def set_filter(self, text):
        self.clear_search_button.setEnabled(text != '')
        self.sort_model.setFilterFixedString(text)

    def clear_filter(self):
        self.clear_search_button.setEnabled(False)
        self.filter_edit.clear()

    def filtered_has_results(self):
        return self.sort_model.rowCount() > 0

    def disable_add_to_playlist(self):
        self.can_add_to_playlist = False
        for action in self.context_menu_actions:
            action.setEnabled(False)

    def enable_add_to_playlist(self):
        self.can_add_to_playlist = True
        for action in self.context_menu_actions:
            action.setEnabled(True)

    def select_song_by_path(self, path):
        items = self.songs_model.findItems(path, column=4)
        assert len(items) > 0
        idx = self.sort_model.mapFromSource(items[0].index())
        if idx.isValid():
            self.set_current_index(idx)
        else:
            self.clear_filter()
            idx = self.sort_model.mapFromSource(items[0].index())
            assert idx.isValid()
            self.set_current_index(idx)

    def select_first(self, silent=False):
        begining = self.sort_model.index(0, 0)
        self.set_current_index(begining, silent)

    def select_next(self, before_removal=False):
        current = self.songs.currentIndex()
        next = self.songs.indexBelow(current)
        if current != next:
            self.set_current_index(next, silent=before_removal)
        else:
            begining = self.sort_model.index(0, 0)
            self.set_current_index(begining, silent=False)

    def select_previous(self):
        current = self.songs.currentIndex()
        next = self.songs.indexAbove(current)
        if current != next:
            self.set_current_index(next)
        else:
            end = self.sort_model.index(self.sort_model.rowCount() - 1, 0)
            self.set_current_index(end)

    def clear_songs(self):
        self.songs_model.removeRows(0, self.songs_model.rowCount())

    def add_songs(self, songs):
        for song in songs:
            row = [QStandardItem(t) for t in song.to_row()]
            self.songs_model.appendRow(row)

        for c in range(Song.COLUMN_COUNT):
            self.songs.resizeColumnToContents(c)

    def ask_should_remove_song(self):
        message = "This song is used by one or more playlists, Deleting it will remove it from them.\n\nDo you want to continue?"
        yes_no = QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No
        result = \
            QMessageBox.warning(self, "Delete Playlist?", message,
                                buttons=yes_no,
                                defaultButton=QMessageBox.StandardButton.No)
        return result == QMessageBox.StandardButton.Yes

    def remove_current(self):
        remove_at = self.songs.currentIndex()
        if self.sort_model.rowCount() == 1:
            self.clear_selection()
        else:
            self.select_next()
        real_idx = self.sort_model.mapToSource(remove_at).row()
        self.songs_model.removeRows(real_idx, 1)

    def filter_text(self):
        return self.filter_edit.text()

    def set_playlist_names(self, playlist_names):
        self.playlist_names = playlist_names
        self.context_menu.clear()

        def add_action(playlist_name):
            action = self.context_menu.addAction(
                        "Add To {}".format(playlist_name))
            action.setEnabled(self.can_add_to_playlist)
            return action

        self.context_menu_actions = \
            [add_action(name) for name in self.playlist_names]

    def custom_context_menu_requested(self, point):
        index = self.songs.indexAt(point)
        item_index = self.sort_model.mapToSource(index) \
                                    .siblingAtColumn(4)
        item = self.songs_model.itemFromIndex(item_index)

        action = self.context_menu.exec_(self.songs.mapToGlobal(QPoint(point)))
        if action:
            playlist_index = self.context_menu_actions.index(action)
            self.add_to_playlist_callback(
                self.playlist_names[playlist_index],
                item.text())

    def disable_add_media(self):
        self.add_files_button.setEnabled(False)
        self.add_directory_button.setEnabled(False)

    def enable_add_media(self):
        self.add_files_button.setEnabled(True)
        self.add_directory_button.setEnabled(True)

    def enable_songs_list(self):
        self.songs.setEnabled(True)

    def disable_songs_list(self):
        self.songs.setEnabled(False)

    def choose_album_dialog(self):
        return QFileDialog.getExistingDirectory(self.main_window,
                                                "Select Music Album",
                                                music_location)

    def choose_tracks_dialog(self):
        return QFileDialog.getOpenFileNames(self.main_window,
                                            "Select Music Files",
                                            music_location,
                                            "Music (*.mp3)")[0]

    def subscribe(self, controller):
        # Add media
        self.add_files_button.clicked.connect(controller.on_add_media_files)
        self.add_directory_button.clicked.connect(
            controller.on_add_media_directory)

        # Item selection
        self.item_changed_callback = controller.on_selected_changed

        # Search
        self.filter_edit.textChanged.connect(controller.on_filter_changed)
        self.clear_search_button.clicked.connect(controller.on_clear_search)

        # Playlist actions
        self.add_to_playlist_callback = controller.on_add_to_playlist
