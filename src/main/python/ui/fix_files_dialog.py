from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, \
                            QPushButton, QLabel, QSizePolicy, QComboBox, \
                            QLineEdit, QLayout, QTextEdit
from PyQt5.QtCore import QSize


class FixFilesDialog(object):
    def __init__(self, parent, missing_artist, missing_artist_and_album):
        self.dialog = QDialog(parent)
        self.dialog.setModal(True)
        self.dialog.setWindowTitle("Fix Misplaced Files?")

        vertical_layout = QVBoxLayout(self.dialog)
        vertical_layout.setContentsMargins(11, 11, 11, 11)
        vertical_layout.setSpacing(6)
        self.dialog.layout().setSizeConstraint(QLayout.SetFixedSize)

        question = "Expected all .mp3 files to have a path of the form {artist}/{album} but found the files listed below.\n Would you like to fix this by moving them to the 'Various Artists' directory?"
        self.question_label = QLabel(question)
        self.question_label.setWordWrap(True)
        self.question_label.setMinimumSize(QSize(750, 10))
        vertical_layout.addWidget(self.question_label)

        self.bad_files_text = QTextEdit(self.dialog)
        self.bad_files_text.setReadOnly(True)
        message = '\n'.join(str(p) for p in missing_artist) + '\n' + \
                  '\n'.join(str(p) for p in missing_artist_and_album)
        self.bad_files_text.setLineWrapMode(QTextEdit.LineWrapMode.NoWrap)
        self.bad_files_text.setText(message)
        vertical_layout.addWidget(self.bad_files_text)

        def make_action_layout():
            action_layout = QHBoxLayout()
            self.no_button = QPushButton("No", self.dialog)
            self.no_button.clicked.connect(self.dialog.reject)
            action_layout.addWidget(self.no_button)

            self.yes_button = QPushButton("Yes", self.dialog)
            self.yes_button.clicked.connect(self.dialog.accept)
            action_layout.addWidget(self.yes_button)
            return action_layout

        action_layout = make_action_layout()
        vertical_layout.addLayout(action_layout)

    def exec(self):
        return self.dialog.exec() == QDialog.DialogCode.Accepted
