def partition(predicate, items):
    is_true = []
    is_false = []
    for item in items:
        (is_true if predicate(item) else is_false).append(item)
    return (is_true, is_false)
