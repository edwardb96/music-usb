from os import listdir, stat, remove, rmdir, mkdir
from os.path import isfile, isdir, join, exists
from pathlib import Path
from ref import Ref
from .progress import copy_file_with_progress


class RemoveFile(object):
    def __init__(self, path):
        self.path = path

    def __str__(self):
        return "-{}".format(self.path)

    __repr__ = __str__


class RemoveDir(object):
    def __init__(self, path):
        self.path = path

    def __str__(self):
        return "-{}".format(self.path)

    __repr__ = __str__


class CopyFile(object):
    def __init__(self, src, dest):
        self.src = src
        self.dest = dest

    def __str__(self):
        return "+{} from {}".format(self.dest, self.src)

    __repr__ = __str__


class CopyDir(object):
    def __init__(self, src, dest):
        self.src = src
        self.dest = dest

    def __str__(self):
        return "+{} from {}".format(self.dest, self.src)

    __repr__ = __str__


def listdir_if_exists(path):
    return listdir(path) if exists(path) else []


def has_different_mtime(a, b):
    return stat(a).st_mtime != stat(b).st_mtime


def sync_and_cost(master_dir, slave_dir):
    instructions = sync(master_dir, slave_dir)
    cost = estimate_cost(instructions)
    return (instructions, cost)


def sync(master_dir, slave_dir):
    (dc, fc, fr, dr) = sync_diff(master_dir, slave_dir)
    fc.extend(sync_updated_playlists(master_dir, slave_dir))
    return (dc, fc, fr, dr)


def sync_updated_playlists(master_dir, slave_dir):
    # some playlist might be missing
    master_items = \
        sorted([p for p in listdir(master_dir) if p.endswith('.m3u')])
    slave_items = \
        sorted([p for p in listdir(slave_dir) if p.endswith('.m3u')])

    master_iterator = iter(master_items)
    slave_iterator = iter(slave_items)

    master_item = next(master_iterator, None)
    slave_item = next(slave_iterator, None)

    file_copies = []

    def slave_path_to(item):
        return join(slave_dir, item)

    def master_path_to(item):
        return join(master_dir, item)

    def copy_to_slave(master_path_to_item):
        file_copies.append(CopyFile(master_path_to_item, slave_dir))

    while True:
        if master_item is not None:
            if slave_item is not None:
                if master_item < slave_item:
                    # exists on master but not on slave - copied to slave
                    master_item = next(master_iterator, None)
                elif master_item > slave_item:
                    # exists on slave but not on master - removed from slave
                    slave_item = next(slave_iterator, None)
                else:
                    # same file - copy if different mtime
                    slave_path_to_item = slave_path_to(master_item)
                    master_path_to_item = master_path_to(master_item)
                    if has_different_mtime(master_path_to_item,
                                           slave_path_to_item):
                        copy_to_slave(master_path_to_item)

                    master_item = next(master_iterator, None)
                    slave_item = next(slave_iterator, None)

            else:
                return file_copies
        else:
            return file_copies


def sync_diff(master_dir, slave_dir):
    """
    master = drive
    slave = backup
    returns instructions to get from slave to master
    """
    def __sync_diff(master_dir, slave_dir, relative_path):
        relative_path_str = '' if relative_path == Path('') \
                               else str(relative_path)
        master_items = sorted(listdir_if_exists(
                              join(master_dir, relative_path_str)))

        slave_items = sorted(listdir_if_exists(
                             join(slave_dir, relative_path_str)))

        master_iterator = iter(master_items)
        slave_iterator = iter(slave_items)

        master_item = next(master_iterator, None)
        slave_item = next(slave_iterator, None)

        file_removals = []
        dir_removals = []
        dir_copies = []
        file_copies = []

        slave_directory_path = join(slave_dir, relative_path_str)

        def slave_path_to(item):
            return join(slave_dir, join(relative_path_str, item))

        def master_path_to(item):
            return join(master_dir, join(relative_path_str, item))

        def extend_actions(actions):
            (dc, fc, fr, dr) = actions
            file_removals.extend(fr)
            dir_removals.extend(dr)
            dir_copies.extend(dc)
            file_copies.extend(fc)

        def copy_to_slave(item):
            master_path = master_path_to(item)
            if isfile(master_path):
                file_copies.append(CopyFile(master_path, slave_directory_path))
            elif isdir(master_path):
                dir_copies.append(CopyDir(master_path, slave_path_to(item)))
                extend_actions(__sync_diff(
                    master_dir, slave_dir, relative_path / item))

        def remove_from_slave(item):
            slave_path = slave_path_to(item)
            if isfile(slave_path):
                file_removals.append(RemoveFile(slave_path))
            elif isdir(slave_path):
                dir_removals.append(RemoveDir(slave_path))
                extend_actions(__sync_diff(
                    master_dir, slave_dir, relative_path / item))

        while True:
            if master_item is not None:
                if slave_item is not None:
                    if master_item < slave_item:
                        # exists on master but not on slave
                        copy_to_slave(master_item)
                        master_item = next(master_iterator, None)
                    elif master_item > slave_item:
                        # exists on slave but not on master
                        remove_from_slave(slave_item)
                        slave_item = next(slave_iterator, None)
                    else:
                        # same at this level, if dir check contents
                        if isdir(master_path_to(master_item)):
                            extend_actions(
                                __sync_diff(master_dir,
                                            slave_dir,
                                            relative_path / master_item))
                        master_item = next(master_iterator, None)
                        slave_item = next(slave_iterator, None)
                else:
                    # rest exist on master but not on slave
                    copy_to_slave(master_item)
                    for item in master_iterator:
                        copy_to_slave(item)
                    master_item = None
                    slave_item = None
            else:
                if slave_item is not None:
                    # file/s exists on slave but not on master
                    remove_from_slave(slave_item)
                    for item in slave_iterator:
                        remove_from_slave(item)
                    master_item = None
                    slave_item = None
                else:
                    return (dir_copies, file_copies,
                            file_removals, dir_removals)

    (dir_copies, file_copies, file_removals, dir_removals) = \
        __sync_diff(master_dir, slave_dir, Path(''))

    dir_removals.reverse()  # recorded top-down, need to happen bottom up
    return (dir_copies, file_copies, file_removals, dir_removals)


def estimate_cost(instructions):
    (dir_copies, file_copies, file_removals, dir_removals) = instructions
    dir_copy_cost = len(dir_copies)
    dir_removal_cost = len(dir_removals)
    file_removal_cost = len(file_removals)
    file_copy_cost = sum(map(lambda cp: stat(cp.src).st_size, file_copies))
    return dir_copy_cost + dir_removal_cost + \
        file_removal_cost + file_copy_cost


def run_instructions_with_progress(instructions, report):
    (dir_copies, file_copies, file_removals, dir_removals) = instructions
    banked_progress = 0
    file_progress = Ref(0)

    for cpy in dir_copies:
        mkdir(cpy.dest)
        banked_progress += 1
        report(banked_progress)

    def on_progress(n):
        file_progress.value = n
        total = banked_progress + file_progress.value
        report(total)

    for cpy in file_copies:
        copy_file_with_progress(cpy.src, cpy.dest, on_progress)
        banked_progress += file_progress.value
        file_progress.value = 0

    for rm in file_removals:
        remove(rm.path)
        banked_progress += 1
        report(banked_progress)

    for rm in dir_removals:
        rmdir(rm.path)
        banked_progress += 1
        report(banked_progress)
