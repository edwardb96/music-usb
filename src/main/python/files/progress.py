import os
import shutil

from ref import Ref


def copy_files_with_progress(src_dst_pairs, on_progress):
    banked_progress = 0
    file_progress = Ref(0)

    def on_file_progress(n):
        file_progress.value = n
        total = banked_progress + file_progress.value
        on_progress(total)

    for src, dst in src_dst_pairs:
        copy_file_with_progress(src, dst, on_file_progress)
        banked_progress += file_progress.value
        file_progress.value = 0


def copyfile(src, dst, on_progress, *, follow_symlinks=True):
    """Copy data from src to dst.

    If follow_symlinks is not set and src is a symbolic link, a new
    symlink will be created instead of copying the file it points to.

    """
    if shutil._samefile(src, dst):
        raise shutil.SameFileError(
                  "{!r} and {!r} are the same file".format(src, dst))

    for fn in [src, dst]:
        try:
            st = os.stat(fn)
        except OSError:
            # File most likely does not exist
            pass
        else:
            # XXX What about other special files? (sockets, devices...)
            if shutil.stat.S_ISFIFO(st.st_mode):
                raise shutil.SpecialFileError("`%s` is a named pipe" % fn)

    if not follow_symlinks and os.path.islink(src):
        os.symlink(os.readlink(src), dst)
    else:
        with open(src, 'rb') as fsrc:
            with open(dst, 'wb') as fdst:
                copyfileobj(fsrc, fdst, callback=on_progress)
    return dst


def copyfileobj(fsrc, fdst, callback, length=16*1024):
    copied = 0
    while True:
        buf = fsrc.read(length)
        if not buf:
            break
        fdst.write(buf)
        copied += len(buf)
        callback(copied)


def copy_file_with_progress(src, dst, on_progress, *, follow_symlinks=True):
    if os.path.isdir(dst):
        dst = os.path.join(dst, os.path.basename(src))
    copyfile(src, dst, on_progress, follow_symlinks=follow_symlinks)
    shutil.copymode(src, dst)
    return dst
