from shutil import move


def delete_up_until_not_empty(file):
    assert file.is_file(), "The path {} is not a file".format(str(file))
    directory = file.parent
    file.unlink()
    while next(directory.iterdir(), None) is None:
        parent = directory.parent
        directory.rmdir()
        directory = parent


def move_clear(source_file_path, dest_path):
    move(source_file_path, dest_path)
    if next(source_file_path.parent.iterdir(), None) == None:
        source_file_path.parent.rmdir()
