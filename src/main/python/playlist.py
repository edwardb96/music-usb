class Playlist(object):
    def __init__(self, name, relative_path):
        self.name = name
        self.songs = []
        self.relative_path = relative_path

    def append(self, song):
        self.songs.append(song)

    def pop(self, n):
        return self.songs.pop(n)

    def reorder(self, from_, to):
        swapped = self.songs.pop(from_)
        self.songs.insert(to, swapped)

    def before_remove(self):
        for song in self.songs:
            song.in_playlists.pop(self.name)

    def remove_uses_of_song(self, song):
        indexes = []
        songs = []
        for i, s in enumerate(self.songs):
            if s == song:
                indexes.append(i)
            else:
                songs.append(s)
        self.songs = songs
        return indexes

    def __bool__(self):
        return bool(self.songs)

    def __iter__(self):
        return iter(self.songs)

    def __getitem__(self, index):
        return self.songs[index]

    def __len__(self):
        return len(self.songs)

    def __repr__(self):
        return "Playlist(name = {})".format(self.name)


def save_playlist(root_dir, playlist):
    absolute_path = root_dir / playlist.relative_path
    with open(str(absolute_path), "w+") as file:
        file.writelines(str(song.relative_path) + "\n" for song in playlist.songs)


def load_playlist(root_dir, songs_index, filepath):
    def is_comment_line(s):
        return s.startswith('#')

    relative_path = filepath.relative_to(root_dir)
    with open(filepath, "r") as file:
        playlist = Playlist(relative_path.stem, relative_path)
        missing_songs = set()
        for line in file.read().splitlines():
            line = line.strip()
            if not is_comment_line(line):
                if line in songs_index:
                    song = songs_index[line]
                    playlist.append(song)
                    song.added_to_playlist(playlist)
                else:
                    missing_songs.add(line)
        return (playlist, missing_songs)
