from pathlib import Path
from re import compile


class Song(object):
    def __init__(self, metadata):
        self.metadata = metadata
        self.relative_path = metadata.storage_relative_path()
        self.in_playlists = {}

    def to_row(self):
        def none_to_empty(s):
            return s if s is not None else ''

        return [self.metadata.track_title,
                self.metadata.album,
                none_to_empty(self.metadata.artist),
                none_to_empty(self.metadata.album_artist),
                str(self.relative_path)]

    def added_to_playlist(self, playlist):
        if playlist.name in self.in_playlists:
            self.in_playlists[playlist.name] += 1
        else:
            self.in_playlists[playlist.name] = 1

    def removed_from_playlist(self, playlist):
        if self.in_playlists[playlist.name] == 1:
            self.in_playlists.pop(playlist.name)
        else:
            self.in_playlists[playlist.name] -= 1

    def is_used_in_playlists(self):
        return bool(self.in_playlists)
        
    def used_in_playlists(self):
        return self.in_playlists.keys()

    def __eq__(self, o):
        return self.relative_path == o.relative_path

    def __ne__(self, o):
        return not self == o

    def __repr__(self):
        return self.metadata.__repr__()

    COLUMN_COUNT = 5


class SongMetadata(object):
    def __init__(self, artist, album_artist, album, track_title):
        self.artist = artist
        self.album_artist = album_artist
        self.album = album
        self.track_title = track_title

    def can_merge(self, other):
        can_merge_album_artist = self.album_artist == other.album_artist \
            if self.album_artist and other.album_artist else True
        can_merge_artist = self.artist == other.artist \
            if self.artist and other.artist else True
        can_merge_track_title = \
            can_merge_titles(self.track_title, other.track_title)

        return can_merge_artist and can_merge_album_artist and \
            can_merge_track_title and self.album == other.album

    def merge(self, other):
        return \
            SongMetadata(
                artist=self.artist or other.artist,
                album_artist=self.album_artist or other.album_artist,
                album=self.album,
                track_title=merge_titles(self.track_title, other.track_title))

    def storage_relative_path(self):
        artist = Path(self.album_artist or self.artist)
        album = Path(self.album)
        title = Path(self.track_title + '.mp3')
        return artist / album / title

    def __eq__(self, other):
        return \
            self.artist == other.artist and \
            self.album_artist == other.album_artist and \
            self.album == other.album and \
            self.track_title == other.track_title

    def __repr__(self):
        return 'SongMetadata(title="{}", album="{}", artist="{}", album_artist="{}")' \
                .format(self.track_title, self.album,
                        self.artist, self.album_artist)


TRACK_NUMBER_TITLE_PATTERN = compile('[0-9]+ (?:- )?(.*)')


def has_track_number_prefix(title):
    return bool(TRACK_NUMBER_TITLE_PATTERN.match(title))


def merge_titles(lhs, rhs):
    return lhs if has_track_number_prefix(lhs) else rhs


def remove_track_number_prefix(title):
    return TRACK_NUMBER_TITLE_PATTERN.match(title)[1]


def can_merge_titles(lhs, rhs):
    if lhs == rhs:
        return True
    else:
        if has_track_number_prefix(lhs):
            return rhs == remove_track_number_prefix(lhs)
        else:
            if has_track_number_prefix(rhs):
                return lhs == remove_track_number_prefix(rhs)
            else:
                return False
